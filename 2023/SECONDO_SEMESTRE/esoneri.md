* Lunedì 17, mercoledì 19, venerdì 14 e 21 Aprile
* Il lunedì:
  * Lab meccanica in aula 4 si sposta alle 12-14 (OK)
  * Chimica in aula 3 non si tiene (OK)
  * Sciarrino in aula 6 non si tiene (OK)
  * In aula 7 e Amaldi c'è già lezione di elettromagnetismo quindi non servono cambi (OK)
* Il mercoledì:
  * Giagu in aula 3 ha finito (OK)
  * Pisano in aula 4 -> 2/5/8 (OK ma deve confermare)
  * Modelli e metodi (Esposito) in Amaldi non si tiene (OK)
* Il venerdì:
  * Pisano in aula 6 -> 2/5/8 (OK ma deve confermare)
  * Di Leonardo salta la lezione il 14, il 21 va in Conversi (OK)
  * Genetica ed evoluzione in Conversi/Rasetti (OK)
  
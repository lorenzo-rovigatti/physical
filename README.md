# physical

Routines to help prepare the schedule of lessons and exams at the Physics Department of the Sapienza University of Rome.

## Requirements

The requirements can be installed with the following command:

`pip3 install --user jupyter pandas openpyxl jinja2`

## Lectures calendar

Here is the list of step required to generate the calendar (and accompanying files):

1. Prepare the [lecture data file](#lecture-data-file)
2. Write a [python script](#python-script) that uses the `physical` python package to fill the calendar
3. Run the script to generate the output files

In practice, my advice is to use the files used to generate the calendar of the previous academic year and update them.

### Lecture data file

To prepare a lecture calendar you first need to update the worksheet `lectures_data.xlsx`. You can also upload it on your Google Drive and collaboratively edit it as a Google Sheet but at the end you need to download it in your working directory as an .xlsx file.
The worksheet contains four main Sheets:

1. **Courses I sem** : 1st semester courses list
2. **Courses II sem**: 2nd semester courses list
3. **Classrooms**: list of classrooms
4. **Groups**: student groups

#### Courses sheets

Everything that should end up on the calendar must be specified here, from courses to meetings. The columns refer to:

1. **short name**: a unique short name to use as a reference for the course (avoid spaces).
2. **full name**: the full course name to be displayed on the calendar
3. **student group**: must be one of the student groups specified in the sheet **Groups**.
4. **tags**: tags are mainly used for courses of *Laurea Magistrale* to help identifying possible overlaps between courses within the same interest group. Tag values: **A** - Astrophysics, **M** - Condensed matter, **T** - Theory, **P** - Particles, **B** - Biosystems, **O** - ?
5. **teacher**: teacher names as displayed in the calendar. If the course is split into channels, this field should contain the comma-separated list of teachers. The number of teachers must match the number of channels in the specified student group. Use "/" to separate the names of teachers that will teach the same course.
6. **hours**: the total number of hours that will be devoted to frontal lectures. It also comprises laboratory hours.
7. **students**: the estimated number of students that will attend the course. This is used to check the capacity of the classrooms where the lessons are held.

#### Classrooms

Here the list of classrooms where lectures will be held is specified. The columns refer to:

1. **codice aula**: a unique short name used to identify (and specify) the classroom. It should not contain spaces.
2. **nome aula**: the full name of the classroom. Used in the output `html` files.
3. **edificio**: the building where the classroom is.
4. **capienza**: the capacity of the classroom.
5. **meet**: the Google Meet link of the classroom (used during the Covid pandemic).
5. **zoom**: the Zoom link of the classroom (used during the Covid pandemic).

#### Groups

1. **Group ID**: a unique code that defines a student group. The code relies on the fact that IDs of Triennale student groups begin with `T`, while student groups of Magistrale have IDs starting with `M`. The color coding is not important.
2. **description**: the full description of the student group.
3. **channels**: comma-separated list of channel headings (*i.e.* the range of the initials of the students belonging to the group).  This field should be empty for student groups that are not split into channels .
4. **student_fractions**: comma-separated list of fractions of students belonging to each channel. The sum of the fractions should add up to unity.

### Python script

The python script should first of all import the `physical` module:

```python
from physical import physical
```

and then create a new schedule:

```python
schedule = physical.Schedule("lectures_data.xlsx", semester=2, semester_start=dt.date(2023, 2, 23), semester_end=dt.date(2023, 6, 13))
```

where the constructor takes 4 mandatory arguments:

1. The path to the lecture data file
2. The semester (this should be either 1 or 2)
3. The first day of the semester
4. The final day of the semester

The last two parameters can be easily specified by using the `datetime` module (here imported with `import datetime as dt`).

#### Specifying holidays and pauses

In order to evaluate correctly the number of hours allocated for each course, it is possible to specify academic holidays (which apply to all courses) or days during which specific courses will be paused. 

For holidays use `schedule.add_holidays(starts_on, ends_on=None)`. Here are two examples:

```python
schedule.add_holidays("2023-06-02") # single day
schedule.add_holidays("2023-04-06", "2023-04-11") # both extremes are included
```

For pauses use `schedule.add_pause(course_name, starts_on, ends_on=None, channel=None)` or  `schedule.add_pauses(courses, starts_on, ends_on=None)`. Use the former to pause single courses, the latter for multiple courses. Some examples:

```python
schedule.add_pause("metodi", "2022-04-28", channel=0) # pause a single course for a day
schedule.add_pauses(schedule.courses_by_group(["T1"]), "2022-04-22", "2022-04-24") # pause all first-year courses for 3 days 
```

#### Add lectures and meetings to the calendar

Classrooms are booked by using the `schedule.add_lectures(course_name, slots, classrooms, channel=None, comment="", check_teacher_overlap=True` method. Here is a rundown of the parameters:

* `course_name`: the **short name** of the course/meeting that will be associated to the booking, as specified in the lectures data file.
* `slots`: a (python) list of time slots. Each slot specifies a day, a starting and end times and (optionally) a range of weeks as a string composed by three (space-separated) fields:
  1. The day should be one of `LUN`, `MAR`, `MER`, `GIO`, `VEN`
  2. The starting and end times are specified as `START-END`, where `START` and `END` are integers (*e.g.* `14-16`)
  3. The weeks during which the classroom should be booked, specified as a range (*e.g.* `2-10` or `4-end`). If not specified, the classroom will be booked for the whole semester.
* `classrooms`: the list of classrooms to be booked.
* `channel`: the channel of the course that will be associated to the booking. It should be `None` for un-channeled courses.
* `comment`: additional text that will be displayed on the calendar. Useful to specify additional information. Mostly used when the class is split into sub-classes to attend lab experiences.
* `check_teacher_overlap`. If set to `False` no `TeacherOverlapException` will be raised by this call. This is useful in those lab courses (mostly computer labs) that have overlapping lab experiences.

Note that `classrooms` should be a list for legacy reasons. 99% of the time you'll use a single-element list.

Some examples:

```python
schedule.add_lectures("mec", ["LUN 8-10", "MAR 14-16", "MER 10-11", "GIO 16-18", "VEN 12-14"], ["Aula3"], channel=1) # book the Aula3 classroom for the first channel of the "mec" course for the whole semester
schedule.add_lectures("atmosfera", ["MAR 8-10", "VEN 8-10"], ["Rasetti"]) # book a room for two slots for an un-channeled course
schedule.add_lectures('eng2',["VEN 17-19"], ["Amaldi"], check_teacher_overlap=False) # the teacher of eng2 is often not assigned yet during the drafting of the calendar. Since there are other "TBA" teachers, use check_teacher_overlap=False to avoid issues
```

#### Print the output

The code can print four different output files:

* `schedule.export_html(path, provisional=True, with_streaming_links=False)` prints the calendar to `path`. If `provisional == True`, the output html file also displays a banner warning users that the calendar has not been finalised yet. If `with_streaming_links == True`, Meet and Zoom links will be shown for each classroom.
* `schedule.report(path)` prints a report file that lists the number of hours assigned to each course, as well as tables that make it easier to spot overlaps between related courses.
* `schedule.simulation(path)` prints an html file that can be used to find overlaps by adding to the calendar courses one by one.
* `schedule.capacity_analysis(path, years_forward=0)` prints a report about possible issues with the classrooms assigned to each course. The number of students attending each course can also be extrapolated to future years and used to estimate future capacity issues. The extrapolating function considers that the number of students of Triennale enrolling in year $y$ can be estimated by $N_T(y) = 180 + 13.5 (y - 2001)$, and that of Magistrale by $N_M(y) = 100 + 7 (y - 2001) + 20$ (see the `extrapolate_N_students` function in the `physical.py`).

Here is a snippet that prints all possible outputs:

```python
version = "1.3"
schedule.export_html(f"2023/SECONDO_SEMESTRE/v{version}_orario.html", provisional=True)
schedule.report(f"2023/SECONDO_SEMESTRE/v{version}_report.html")
schedule.simulation(f"2023/SECONDO_SEMESTRE/v{version}_simulazione.html")
schedule.capacity_analysis(f"2023/SECONDO_SEMESTRE/v{version}_capacity.html")
schedule.capacity_analysis(f"2023/SECONDO_SEMESTRE/v{version}_capacity_extrapolated_3.html", years_forward=3)
```


## Exams calendar

For every year you need a separate copy of [this Google Spreadheet](https://docs.google.com/spreadsheets/d/1xE-3dZvW2s0ZMT3rdf0xKhr4-NhXb4auAX8pA9RzpIM/edit?usp=sharing). Rename the spreadsheet preserving the format
**Esami-triennale_YYYY/YYYY** and replacing **YYYY/YYYY** with the current academic year, *e.g.* **2018/2019**.
The first column has conditional formatting and changes color according to the year number (1,2 or 3). Do not change background color manually. The calendar can be exported in html format for online publishing using the function **Export as HTML** under the menu **Exams**. The exported html will be saved in your Google Drive home directory.

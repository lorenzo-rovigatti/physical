* Giardina non può avere lezione martedì 16-18 perché coordina i seminari generali

Risoluzione problemi Malusa:
* Swap Malusa <-> Crismale (I e III canale)
* Martedì swap tra analisi e geometria (da 10-12/8-10 a 8-10/10-12)

## Cambiamenti

### 02/08/2021

* Many Body Physics (Grilli), MAR 14-15 Careri -> Amaldi
* GIO 14-16 la Majorana non è più dedicata agli studenti

### 03/08/2021

* Physics of Solids (Mauri), MAR 9-12 Aula 8 -> MAR 12-14 Rasetti e VEN 12-14 Conversi -> VEN 12-15 Conversi
* Astronomia (Melchiorri), VEN 14-15 Conversi -> VEN 15-16 Conversi
* Sistemi Autogravitanti (Merafina), MAR 16-18 cambio aula da Aula 6 ad Aula 8
* Surface Physics and Nanostructures (Mariani), MAR 16-18 cambio aula da Aula 5 ad Aula 6

### 04/08/2021

* Experimental Gravitation (Ricci), MAR 10-11 -> MER 10-11 (sempre Aula 5)
* Laboratorio di Calcolo Avanzato (Puppo). Era MER 10-12 e VEN 14-16 in Aula 8, ora è MAR 10-12 in Aula 8, VEN 10-12 in Aula 5 e MAR 12-14 nel lab di matematica (che prenota direttamente la Puppo).

## Nuova versione (v7)

### 31/08/2021

* Non-linear Quantum Optics (Mataloni), LUN 14-15 -> LUN 9-10 (sempre in Conversi)
* Solid-state Sensors (Polimeni), LUN 8-10 da Conversi a Careri
* Evoluzione Chimica dell'Universo (da assegnare), VEN 12-14 Aula 5 -> VEN 14-16 Aula 8

### 03/09/2021 

* Computational Biophysics (Giansanti), VEN 8-9 -> LUN 8-9 (sempre in Aula 5)

### 07/09/2021

* Physics laboratory 1 condmat (Mariani), MAR 10-12 da Majorana a Conversi
* Laboratorio di Fisica Computazionale I (Pani), MER 9-10 -> MAR 11-12 (sempre in Majorana)
* Astronomia (Melchiorri) GIO 8-10 -> MAR 8-10 (sempre in Conversi)
* Astronomia (Melchiorri) VEN 15-16 -> GIO 8-9 (sempre in Conversi)
* Computing Methods for Physics 1 (Pannarale) MAR 8-10 passa da Conversi a Majorana
* Quantum Information and Computation (Sciarrino) MAR 8-10 passa da Majorana a Rasetti
* Fisica Superiore (Pelissetto), LUN 15-16 Conversi -> MAR 10-11 Majorana
* Computing Methods for Physics 4 (De Michele) LUN 12-14 -> LUN 14-16 (sempre in Conversi)
* Laboratorio di Calcolo 4 (Boeri) MAR 14-17 -> VEN 14-17 (sempre in Aula 17)
* Astrofisica (De Bernardis) MER 10-12 -> MER 10-11, l'ora che togliamo la mettiamo allo slot di lunedì, che diventa 16-18 (sempre in Amaldi)
* Fluidodinamica per l'Astrofisica (Capuzzo Dolcetta) MER 12-13 -> MER 11-12 (sempre in Amaldi)

### 19/09/2021

* Biochimica (Boffi), VEN 16-18 -> VEN 14-16 (sempre in Rasetti)
* Relatività generale (Pani), VEN 16-18 -> VEN 14-16 (sempre in Amaldi)
* Condensed Matter Physics (entrambi i canali), VEN 14-16 -> VEN 16-18 (un canale è in Amaldi, l'altro in 7)
* Tutte le esercitazioni di Laboratorio di Fisica Computazionale I confluiscono in un'unica prenotazione: VEN 16-19 in Aula 17. Le altre vanno cancellate.
* Esercitazioni di Laboratorio di Calcolo (Soffi), VEN 14-17 -> GIO 14-17 (sempre in sala calcolo)
* Esercitazioni di Laboratorio di Calcolo (Boeri), VEN 14-17 -> GIO 14-17 (sempre in Aula 17)
* Introduzione alla Teoria dei Processi Stocastici (Leuzzi), VEN 15-16 passa da Rasetti a Careri

### 22/09/2021

* Surface Physics and Nanostructures (Mariani), MER 15-16 Aula 5 -> MER 9-10 Rasetti
* Surface Physics and Nanostructures (Mariani), GIO 10-12 passa da Aula 5 a Sala Calcolo
* Collider Particle Physics (Bagnaia), MER 13-14 -> MER 13-15 (si aggiunge un'ora), sempre in Aula 8

### 23/09/2021

* Introduction to Particle Physics (Luci), LUN 14-16 Aula 6 -> LUN 16-18 Aula 5
* Medical Applications of Physics (Saini), MER 14-15 -> MER 14-16 (si aggiunge un'ora) sempre in Aula 5

### 27/09/2021

* Termodinamica e Laboratorio (Di Leonardo), LUN 14-16 Aula 4 -> LUN 14-16 Aula 6
* Computational Biophysics (Giansanti), LUN 8-9 e GIO 8-10 passano da Aula 5 a Majorana

### 28/09/2021

* Cosmologia Teorica (Maoli), GIO 14-16 Aula 8 -> VEN 16-18 Careri
* Experimental Gravitation (Ricci), VEN 8-10 passa da Aula 8 ad Aula 5

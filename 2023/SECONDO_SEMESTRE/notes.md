* English Language ha bisogno di 30-40 posti e preferisce non stare in un'aula grande del Fermi
* Modelli e Metodi Matematici per la Fisica (Astro) vorrebbe mantenere lo stesso orario e la stessa aula del 2021/2022 (Conversi, MAR 8-10, MER 13-14, GIO 10-12, VEN 8-10)
* Astrofisica Stellare (Paolo Ventura) Volevo segnalare, al fine di evitare sovrapposizioni, che il giovedì e il venerdì, dalle 8 alle 10, terrò il corso a Roma Tre (come da 10 anni a questa parte). Pertanto, sarei felicissimo di tenere le mie lezioni il giovedì e il venerdì, dal momento che verrò a Roma comunque, ma non nella prima parte della mattinata. Volevo anche chiedere, se possibile, di risparmiarmi il martedì, giorno in cui si tengono i seminari all'Osservatorio di Roma, dove lavoro. Sono peraltro io l'organizzatore dei seminari.
* Fa Bellini (I canale di Meccanica, ??? canale di FNSN)
  1)Il mercoledi’ e giovedi’ mattina devo accompagnare i figli a scuola e per stare sicuri di arrivare in tempo ti chiederei se possibile di non fare lezione prima delle 10.
  2)Martedi' e mercoledi' pomeriggio se potessi non finire tardi ugualmente mi aiuterebbe nel gestire i bambini.
  3)Avere il mercoledi’ libero sarebbe ideale ma con meccanica mi pare non sia possibile e cmq ho un esercitatore con cui mi organizzerò.
* Gravitational Waves Compact Stars and Black Holes dovrebbe contenere una virgola che il codice non gestisce bene

## 16/11/2022

* ven 12-13 aula 6 e 7 mettere "impegno didattico" al posto di EM

## 20/11/2022

* Giampaolo Pisano non può insegnare la mattina presto

## 15/12/2022

* **D. Del Re**: Gli orari dei miei due corsi sono simili a quelli dell’anno scorso e vanno bene. C’e’ solo un aspetto critico che ho notato l’anno scorso: ho lezione di meccanica il giovedi’ alle 4-6 pm e venerdi’ mattina 8-10 e gli studenti non fanno in tempo a tornare a casa (in particolare quelli che abitano lontano) che gia’ devono ripartire la mattina dopo all’alba senza manco aver potuto aprire il quaderno.

## 17/01/2023

* Laboratorio di Elettromagnetismo (Gauzzi): aggiunto turno di laboratorio MER 9-13
* Elettronica Generale (Nicolau): MER 16-18 -> MER 16-17, GIO 14-15 -> LUN 12-14 (sempre in Majorana)

## 20/02/2023

* Modelli e Metodi III canale (Esposito): GIO 9-10 -> GIO 11-12
* Laboratorio di Elettromagnetismo (Di Domenico): GIO 10-12 -> GIO 9-11
* Modelli e Metodi Astrofisica (Cesi): GIO 11-13

## 22/02/2023

* Elettromagnetismo II canale (Piacentini): GIO 12-14 -> GIO 16-18

## 27/02/2023

* English Language: MER 8-11 -> MER 14-16 (sempre in Majorana)

## 06/03/2023

* Elettronica Generale (Nicolau): tutte le lezioni da Majorana -> Aula 8
* Spectroscopy Methods (Lupi): MAR 14-15 (settimane 11-14) da Majorana -> Rasetti
* Methods in experimental particle physics (Di Domenico): tutte le lezioni da Majorana -> Aula 8
* Dinamica dei sistemi stellari (Merafina): tutte le lezioni da Majorana -> Aula 8
* Computer Architecture for Physics (Biagioni/Lonardo): tutte le lezioni da Majorana -> Aula 8
* Lingua Inglese: rimane solo la lezione di lunedì in Rasetti (a cui forse aggiungeremo 1-2 lezioni una tantum)
* English language: MER 14-16 da Majorana -> Aula 8
* English language: VEN 17-19 Amaldi -> VEN 14-16 in Aula 8

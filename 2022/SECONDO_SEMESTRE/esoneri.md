Bisogna organizzare gli esoneri di Meccanica, Elettromagnetismo, Struttura della Materia e Fisica Nucleare e Subnucleare

* I docenti di struttura vorrebbero che l'esonero fosse di mercoledì, 2 ore vanno bene ma 3 sarebbero meglio
* Elettromagnetismo: 3 ore
* Meccanica: 2 ore
* FNSN: 2 ore?

Esempio:

## Primo esonero

### Venerdì 22/04, opzione A

Mettiamo in pausa 
* tutto il primo anno
* tutto il secondo anno
* istituzioni di fisica applicata 
* fisica dell'atmosfera

Quindi il terzo anno fa regolarmente lezione dalle 14 in poi

Gli orari degli esoneri sono:

* 8-10:30 meccanica (Amaldi, 3, 4, 6, 7)
* 10:30-13:00 struttura (Amaldi, 3, 6, 7)

### Venerdì 22/04, opzione B

Mettiamo in pausa 
* il primo e il terzo canale del primo anno
* tutto il terzo anno

Gli orari degli esoneri sono:

* 13:00 - 15:30 meccanica (Amaldi, 3, 4, 6, 7)
* 15:30 - 18:00 struttura (Amaldi, 3, 6, 7)

## Giovedì 28/04

Mettiamo in pausa
* i corsi del primo anno della mattina (canali 1-3)
* i corsi del secondo anno della mattina (canale 1)
* i corsi del terzo anno della mattina (canali 1 e 2)

Gli orari degli esoneri sono:

* 8-11 elettromagnetismo (Amaldi, 3, 4, 6, 7)
* 12-14 FNSN (3, 4, 6, 7)

## Secondo esonero

### Lunedì 13/06

* 9-12: struttura (3, 4, 6, 7)

### Mercoledì 15/06

* 14-17: FNSN (3, 4, 6, 7)

### Giovedì 16/06

* 13:30-15:30 meccanica (Cabibbo, 3, 4, 6, 7)
* 16-19 elettromagnetismo (Cabibbo, 3, 4, 6, 7)

'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=2, semester_start=dt.date(2022, 2, 23), semester_end=dt.date(2022, 6, 10))

# holidays
schedule.add_holidays("2022-04-25")
schedule.add_holidays("2022-05-01")
schedule.add_holidays("2022-06-02")
schedule.add_holidays("2022-04-14", "2022-04-19")

# T1
schedule.add_lectures("mec", ["LUN 8-10", "MAR 14-16", "MER 11-12", "GIO 10-12", "VEN 14-16"], ["Aula3"], channel=1)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 17-18", "GIO 16-18", "VEN 8-10"], ["Aula3"], channel=2)
schedule.add_lectures("mec", ["LUN 8-10", "MAR 14-16", "MER 11-12", "GIO 10-12", "VEN 14-16"], ["Aula4"], channel=3)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 17-18", "GIO 16-18", "VEN 8-10"], ["Aula4"], channel=4)

schedule.add_lectures("chem", ["LUN 10-12", "MER 10-11", "GIO 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("chem", ["LUN 16-18", "MER 16-17", "GIO 14-16"], ["Aula3"], channel=2)
schedule.add_lectures("chem", ["LUN 10-12", "MER 10-11", "GIO 8-10"], ["Aula4"], channel=3)
schedule.add_lectures("chem", ["LUN 16-18", "MER 16-17", "GIO 14-16"], ["Aula4"], channel=4)

schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 16-18"], ["Aula3"], channel=1)
schedule.add_lectures("labmec", ["MAR 10-12", "MER 14-16", "VEN 10-12"], ["Aula3"], channel=2)
schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 16-18"], ["Aula4"], channel=3)
schedule.add_lectures("labmec", ["MAR 10-12", "MER 14-16", "VEN 10-12"], ["Aula4"], channel=4)

# T2

schedule.add_lectures("metodi", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 10-12", "VEN 8-10"], ["Amaldi", "Aula6", "Aula7"])
schedule.add_lectures("em", ["LUN 16-18", "MAR 10-12", "MER 14-15", "GIO 8-10", "VEN 10-12"], ["Amaldi", "Aula6", "Aula7"])
schedule.add_lectures("labem", ["MER 16-18", "GIO 12-13"], ["Amaldi", "Aula6", "Aula7"])

# T2.A

schedule.add_lectures("metodi.astro", ["MAR 8-10", "MER 13-14", "GIO 10-12", "VEN 8-10"], ["Conversi"])
schedule.add_lectures("eng", ["MER 9-12"], ["Conversi"])

# T3

schedule.add_lectures("ottica", ["LUN 10-12", "MAR 16-18", "MER 9-10"], ["Amaldi", "Aula6", "Aula7"])
schedule.add_lectures("struttura", ["LUN 8-10", "MER 10-12", "VEN 13-14"], ["Amaldi", "Aula6", "Aula7"])
schedule.add_lectures("nucleare", ["MAR 14-16", "MER 8-9", "VEN 14-16"], ["Amaldi", "Aula6", "Aula7"])

# T3.O

schedule.add_lectures("metodi_IA", ["MER 12-14"], ["Aula7"])
schedule.add_lectures("metodi_IA", ["GIO 16-18"], ["LabSS-LabAstro"])

schedule.add_lectures("applicata", ["LUN 12-14", "VEN 10-13"], ["LabCalc"])

schedule.add_lectures("atmosfera", ["MAR 8-10", "VEN 8-10"], ["Rasetti"])

schedule.add_lectures("genevo", ["MAR 12-14", "GIO 8-10"], ["Rasetti"])

schedule.add_lectures("elettronica", ["MAR 10-12", "GIO 10-12"], ["Majorana"])

# M1
schedule.add_lectures('spectra',["MAR 12-14"], ["Aula8"])
schedule.add_lectures('spectra',["MER 14-16"], ["Aula8"])
schedule.add_lectures('cond',["LUN 12-14", "MER 12-14", "GIO 16-17"], ["Careri"])
schedule.add_lectures('particles',["LUN 12-14", "MER 10-12", "MAR 16-17"], ["Rasetti"])
schedule.add_lectures('qed',["VEN 9-10", "GIO 14-16"], ["Careri"])
schedule.add_lectures('qed',["MAR 12-14"], ["Conversi"])
schedule.add_lectures('matphys',["MER 14-16", "GIO 8-10"], ["Conversi"])
schedule.add_lectures('group',["MER 14-16", "GIO 8-10"], ["Careri"])
schedule.add_lectures('neq',["MAR 16-18", "LUN 16-18", "LUN 18-19"], ["Careri"])
schedule.add_lectures('gravity',["MER 8-10", "GIO 10-12"], ["Careri"])
schedule.add_lectures('super',["LUN 10-12", "GIO 12-14"], ["Majorana"])
schedule.add_lectures('super',["VEN 12-13"], ["Careri"])
schedule.add_lectures('nuclear',["VEN 12-14", "MER 8-10"], ["Rasetti"])
schedule.add_lectures('liquids',["VEN 10-12"], ["Aula8"])
schedule.add_lectures('liquids',["MER 8-10"], ["Aula8"])
schedule.add_lectures('neural',["LUN 14-16", "MER 16-18"], ["Rasetti"])
schedule.add_lectures('symm',["MER 16-18", "VEN 10-12"], ["Careri"])
schedule.add_lectures('molbio',["MAR 14-16", "GIO 14-16"], ["aula A - Daniel Bovet"])
# schedule.add_lectures('exppart',["LUN 16-18", "GIO 18-19 5-16", "VEN 16-18 5-16"], ["da coordinare con il Prof. Di Domenico"])
# schedule.add_lectures('labphys2bio',["VEN 12-14 1-6"], ["Aula7"])
# schedule.add_lectures('labphys2bio',["GIO 16-18 1-13"], ["Aula6"])
# schedule.add_lectures('labphys2mat',["GIO 16-19 1-4", "VEN 8-10 1-4"], ["Careri"])
# schedule.add_lectures('labphys2part',["GIO 16-19 1-4", "VEN 16-19 1-4"], ["Rasetti"])
schedule.add_lectures('astroalte',["LUN 14-16", "MAR 8-10"], ["Careri"])

# schedule.add_lectures('extra',["LUN 16-18", "MAR 16-18"], ["Aula6"])
schedule.add_lectures('extra',["LUN 12-14", "GIO 16-18"], ["Aula6"])

schedule.add_lectures('astrotheo',["LUN 10-12", "GIO 12-14"], ["Careri"])
schedule.add_lectures('stellare',["MAR 14-16", "VEN 14-16"], ["Conversi"])

#schedule.add_lectures('astropt',["MAR 12-14", "MER 14-16"], ["Aula7"])
schedule.add_lectures('astropt',["MAR 12-14", "MER 12-14"], ["Aula6"])

schedule.add_lectures('stardyn',["GIO 16-18", "MER 16-18"], ["Aula8"])

schedule.add_lectures('obscosmo',["MER 12-14", "VEN 10-12"], ["Majorana"])

# schedule.add_lectures('physcosmo',["GIO 14-16", "VEN 8-10"], ["Rasetti"])
schedule.add_lectures('physcosmo',["GIO 14-16", "VEN 12-14"], ["Conversi"])

schedule.add_lectures('planets',["LUN 12-14"], ["Majorana"])
schedule.add_lectures('planets',["MAR 10-12"], ["LabCalc"])
schedule.add_lectures('eng2',["LUN 10-13", "LUN 14-17"], ["Aula8"])
schedule.add_lectures('detnacc',["MAR 10-12", "GIO 12-14"], ["Rasetti"])

schedule.add_lectures('compuarch',["LUN 8-10", "MAR 12-14"], ["Majorana"])

# schedule.add_lectures('bioteo',["MAR 8-10", "VEN 14-16"], ["Careri"])
schedule.add_lectures('bioteo',["MAR 10-12", "VEN 14-16"], ["Careri"])

# schedule.add_lectures('phot',["MER 10-12", "LUN 16-18"], ["Aula7"])


schedule.add_lectures('nonlinear',["MAR 14-16", "VEN 14-16"], ["Rasetti"])
schedule.add_lectures('biophys',["MAR 12-14", "VEN 16-18"], ["Careri"])


# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])
schedule.add_lectures("studenti", ["LUN 14-19", "MAR 14-19", "MER 14-19", "GIO 14-19", "VEN 14-19"], ["Majorana"])

schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2"])
# schedule.add_lectures("Chimica", ["LUN 14-18", "MAR 14-18", "MER 14-18", "GIO 14-18", "VEN 14-18"], ["Aula3"])
# schedule.add_lectures("Genetica", ["LUN 16-18", "MAR 14-18", "GIO 14-18", "VEN 16-18"], ["Aula4"])
# schedule.add_lectures("ScienzeAmbientali", ["MAR 14-16"], ["Careri"])
# schedule.add_lectures("ScienzeAmbientali", ["GIO 14-16"], ["Majorana"])

schedule.export_html("2022/SECONDO_SEMESTRE/v1.0_orario.html", provisional=True)
schedule.report("2022/SECONDO_SEMESTRE/v1.0_report.html")
schedule.simulation("2022/SECONDO_SEMESTRE/v1.0_simulazione.html")

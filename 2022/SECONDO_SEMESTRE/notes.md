## Problemi versione 2.1

* ~~De Petris di Ottica Astronomica ha lezione altrove 11-13 il LUN, GIO e VEN~~
* ~~Ballarino di Molecular Biology vorrebbe fare lezione il MAR (GIO OK) perché insegna 11-13 LUN, MER, VEN~~
* ~~Shahram di FNSN va via il VEN alle 15 quindi non può fare lezione 14-16~~
* ~~Pisano (Meccanica III canale) ha problemi con la lezione LUN 8-10~~
* ~~Parisi (Genetica ed Evoluzione) vorrebbe tenere le sue lezioni nella stessa aula~~
* ~~Pentericci (Astrofisica Extragalattica) vorrebbe tenere le lezioni lunedì e martedì~~

## Cambiamenti

### 24/01/2022

* Chimica (IV canale, ora Lanzilotto), LUN 16-18 -> VEN 10-12 (stessa aula)
* Laboratiorio di Meccanica (IV canale, Paramatti), Ven 10-12 -> LUN 16-18 (stessa aula)

### 01/02/2022

* Onde gravitazionali stelle e buchi neri (Gualtieri), aggiunta un'ora GIO 14-15 (sempre in Careri)

### 04/02/2022

* Lingua Inglese (II anno, Nonno), Conversi VEN 13-16 -> Careri MER 9-13 nelle settimane che vanno dal 28/03 al 06/05
* Onde gravitazionali stelle e buchi neri (Gualtieri), MER 8-10: nel periodo tra 28/03 e 06/05 le lezioni si spostano in Majorana
* Methods in experimental particle physics (di Domenico), VEN 14-16 -> VEN 16-18 (stessa aula)
* Physics Laboratory II - PART (Cavoto), Aula 7 VEN 16-18 -> Conversi VEN 14-16

### 07/02/2022

* Condensed Matter Physics II (Boeri), Aula 7 MER 12-14 -> Majorana MER 10-12
* Superconductivity and superfluidity (Benfatto), Majorana MER 10-12 -> Amaldi MER 12-14

### 08/02/2022

* English Language, Conversi LUN 9-12 -> Aula 8 VEN 9-12
* Electroweak Interactions (Contino), GIO 10-12 -> MAR 12-14
* Electroweak Interactions (Contino), Careri MER 14-15 -> Aula 7 MER 12-14 dal 23/02 al 22/04
* Nonlinear waves and solitons (Santini), MAR 14-16 -> MAR 10-12
* Quantum Electrodynamics (Papinutto), MAR 12-14 -> LUN 10-12 (sempre in Conversi)
* Group Theory in Mathematical Physics (Panati), Rasetti GIO 12-14 -> Careri GIO 10-12
* Particle Physics (Bagnaia), LUN 12-14 cambio aula, da Rasetti -> Careri
* Particle Physics (Bagnaia), Rasetti MER 10-12 -> Careri MER 14-16
* Astrofisica Stellare (Schneider): tre lezioni in più in orario VEN 8-10 in Careri il 22/04, 03/06 e 10/06

### 15/02/2022

* Astrofisica delle Alte Energie (Stella), entrambe le lezioni si spostano dalla Rasetti alla Majorana
* Group Theory in Mathematical Physics (Panati), Rasetti VEN 8-10 -> Majorana VEN 8-10
* Introduzione alla fisica dell'atmosfera (Siani), entrambe le lezioni si spostano in Rasetti

### 23/02/2022

* Nonlinear waves and solitons (Santini), Rasetti MAR 10-12 -> Aula 8 LUN 10-12
* Nonlinear waves and solitons (Santini), Rasetti VEN 10-12 -> Careri GIO 12-14

### 28/02/2022

* Nonlinear waves and solitons (Santini), Aula 8 LUN 10-12 -> Careri VEN 16-18

### 06/03/2022

* Condensed Matter Physics II (Boeri), Majorana MER 10-12 -> in aula 5 (stesso orario)
* Physical Cosmology (Melchiorri), Aula 8 GIO 15-16 -> in Careri (stesso orario)
* Physical Cosmology (Melchiorri), Aula 8 MER 14-16 -> in Sala Calcolo (stesso orario)

### 15/03/2022

* Inglese (Nonno), Careri MER 9-13 -> Rasetti LUN 12-14 (dal 21/03 a fine semestre)
* Inglese (Nonno), due lezioni aggiuntive in Aula 8 VEN 12-16 il 21 e 28 Aprile
* Particle physics (Bagnaia), Rasetti MAR 16-17 -> MAR 16-18 (stessa aula)

### 30/03/2022

* English Language:
  * Cancellare le lezioni Conversi LUN 13-16 e Aula 8 VEN 9-12
  * Aggiungere lezione in Aula 6 VEN 17-19
  * Aggiungere lezione in Majorana MER 8-11 dall'11 Maggio (compreso) fino a fine semestre
  
'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=1, semester_start=dt.date(2021, 9, 22), semester_end=dt.date(2022, 1, 12))

# holidays
schedule.add_holidays("2021-11-01")
schedule.add_holidays("2021-12-08")
schedule.add_holidays("2021-12-23", "2022-01-06")

# T1
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 10-12", "VEN 10-12"], ["Aula3"], channel=1)
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 10-12", "VEN 10-12"], ["Aula4"], channel=2)
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 8-10", "VEN 10-12"], ["Aula6"], channel=3)
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 10-12", "VEN 10-12"], ["Aula7"], channel=4)

schedule.add_lectures("geometria", ["MAR 8-10", "MER 9-11", "GIO 12-13", "VEN 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("geometria", ["MAR 8-10", "MER 9-11", "GIO 12-13", "VEN 8-10"], ["Aula4"], channel=2)
schedule.add_lectures("geometria", ["MAR 8-10", "MER 8-10", "GIO 12-13", "VEN 8-10"], ["Aula6"], channel=3)
schedule.add_lectures("geometria", ["MAR 8-10", "MER 9-11", "GIO 12-13", "VEN 8-10"], ["Aula7"], channel=4)

schedule.add_lectures("labcal", ["MER 8-9", "GIO 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("labcal", ["MER 8-9", "GIO 8-10"], ["Aula4"], channel=2)
schedule.add_lectures("labcal", ["MER 10-11", "GIO 10-12"], ["Aula6"], channel=3)
schedule.add_lectures("labcal", ["MER 8-9", "GIO 8-10"], ["Aula7"], channel=4)
# esercitazioni labcalc
schedule.add_lectures("labcal", ["MER 13-16"], ["LabCalc"], channel=1, comment="a")
schedule.add_lectures("labcal", ["LUN 16-19"], ["LabCalc"], channel=1, comment="b")

schedule.add_lectures("labcal", ["LUN 14-17"], ["Aula17"], channel=2, comment="a")
schedule.add_lectures("labcal", ["VEN 14-17"], ["LabCalc"], channel=2, comment="b")

schedule.add_lectures("labcal", ["LUN 10-13"], ["Aula17"], channel=3, comment="b")
schedule.add_lectures("labcal", ["MAR 15-18"], ["LabCalc"], channel=3, comment="a")

schedule.add_lectures("labcal", ["VEN 14-17"], ["Aula17"], channel=4, comment="a")
schedule.add_lectures("labcal", ["MER 16-19"], ["LabCalc"], channel=4, comment="b")

# T2
schedule.add_lectures("vector", ["LUN 8-10", "MER 14-16", "GIO 11-12", "VEN 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("vector", ["LUN 8-10", "MER 10-12", "GIO 10-11", "VEN 10-12"], ["Majorana"], channel=2)
schedule.add_lectures("vector", ["LUN 8-10", "MER 14-16", "GIO 16-17", "VEN 12-14"], ["Aula7"], channel=3)

# meccanica razionale dovrebbero essere 60 ore, quindi 5 ore a settimana
schedule.add_lectures("mecraz", ["LUN 10-12", "MER 13-14"], ["Amaldi", "Majorana", "Aula7"])
schedule.add_lectures("mecraz", ["VEN 8-10"], ["Amaldi"], channel=1)
schedule.add_lectures("mecraz", ["VEN 12-14"], ["Majorana"], channel=2)
schedule.add_lectures("mecraz", ["VEN 14-16"], ["Aula7"], channel=3)

# il primo canale va di martedì perché lunedì altrimenti avrebbe 10 ore filate di lezione+laboratorio
schedule.add_lectures("labfiscomp", ["MAR 10-12", "GIO 10-11"], ["Amaldi"], channel=1)
schedule.add_lectures("labfiscomp", ["LUN 12-14", "MER 12-13"], ["Majorana"], channel=2)
schedule.add_lectures("labfiscomp", ["LUN 12-14", "MER 12-13"], ["Aula7"], channel=3)
# esercitazioni lab fis comp
schedule.add_lectures("labfiscomp", ["GIO 15-18"], ["Aula17"], channel=1, comment="a")
schedule.add_lectures("labfiscomp", ["GIO 15-18"], ["LabCalc"], channel=1, comment="b")
schedule.add_lectures("labfiscomp", ["GIO 12-15"], ["Aula17"], channel=2, comment="all")
schedule.add_lectures("labfiscomp", ["GIO 9-12"], ["Aula17"], channel=3, comment="a")
schedule.add_lectures("labfiscomp", ["GIO 9-12"], ["LabCalc"], channel=3, comment="b")

schedule.add_lectures("termo", ["MAR 12-14", "GIO 8-10"], ["Amaldi"], channel=1)
schedule.add_lectures("termo", ["MAR 12-14", "GIO 8-10"], ["Majorana"], channel=2)
schedule.add_lectures("termo", ["MAR 12-14", "GIO 14-16"], ["Aula7"], channel=3)
schedule.add_lectures("termo", ["LUN 14-18"], ["LabTermo"], channel=1)
schedule.add_lectures("termo", ["MER 14-18"], ["LabTermo"], channel=2)
schedule.add_lectures("termo", ["MAR 15-19"], ["LabTermo"], channel=3)

# T2.A
schedule.add_lectures("astronomia", ["MAR 10-12", "MER 13-14", "GIO 8-10"], ["Conversi"])

# T3
# Vignati è impegnato mercoledì pomeriggio quindi il suo canale ha lezione di martedì
schedule.add_lectures("segnali", ["LUN 10-12", "MAR 16-18"], ["Aula3"], channel=1)
schedule.add_lectures("segnali", ["LUN 10-12", "MER 12-14"], ["Aula4"], channel=2)
schedule.add_lectures("segnali", ["LUN 10-12", "MER 12-14"], ["Aula6"], channel=3)
schedule.add_lectures("segnali", ["GIO 15-19"], ["LabSS-LabAstro"], channel=1)
schedule.add_lectures("segnali", ["MAR 15-19"], ["LabSS-LabAstro"], channel=2)
schedule.add_lectures("segnali", ["LUN 15-19"], ["LabSS-LabAstro"], channel=3)

schedule.add_lectures("quantum", ["LUN 8-10", "MAR 12-14", "GIO 13-14", "VEN 12-14"],["Aula3"], channel=1)
schedule.add_lectures("quantum", ["LUN 8-10", "MAR 12-14", "GIO 13-14", "VEN 12-14"],["Aula4"], channel=2)

schedule.add_lectures("statmech", ["LUN 12-14", "MER 14-16"], ["Aula3", "Aula4", "Aula6"])
schedule.add_lectures("statmech", ["MAR 15-16"], ["Aula3"], channel=1)
schedule.add_lectures("statmech", ["GIO 14-15"], ["Aula4"], channel=2)
schedule.add_lectures("statmech", ["MAR 14-15"], ["Aula3"], channel=3)

# T3.A
# ho tolto 3 lezioni da un'ora da questo slot
schedule.add_lectures("astro", ["MAR 14-15", "MER 10-12", "GIO 12-14"], ["Amaldi"])
schedule.add_lectures("quantstat", ["LUN 10-12", "MAR 10-13", "GIO 10-12", "VEN 8-10"], ["Careri"])
schedule.add_lectures("labastro", ["MAR 16-18", "VEN 10-12"], ["Careri"])
schedule.add_lectures("labastro", ["MER 15-19"], ["LabSS-LabAstro"], comment="all")
# ho tolto 2 lezioni da un'ora da questo slot
schedule.add_lectures("astrofluo", ["LUN 12-14", "MER 12-13", "VEN 12-14"], ["Amaldi"])

# T3.O
schedule.add_lectures("prob", ["LUN 14-15", "MAR 8-10", "MER 8-10"], ["Amaldi"])

# M1
schedule.add_lectures("rqm", ["MAR 14-16", "GIO 14-16"], ["Aula6"])
schedule.add_lectures("rqm", ["VEN 9-10"], ["Conversi"])

schedule.add_lectures("eleweak", ["VEN 8-10"], ["Majorana"])
schedule.add_lectures("eleweak", ["LUN 12-14", "MAR 8-10", "MER 8-10", "GIO 12-14"], ["Conversi"])

schedule.add_lectures("critical", ["LUN 10-11"], ["Rasetti"])
schedule.add_lectures("critical", ["MER 10-12"], ["Conversi"])
schedule.add_lectures("critical", ["GIO 16-18"], ["Careri"])

schedule.add_lectures("condmat.1", ["MAR 16-18", "MER 16-17", "VEN 12-14"], ["Aula6"])
schedule.add_lectures("condmat.2", ["MAR 16-18", "MER 16-17", "VEN 14-16"], ["Amaldi"])

schedule.add_lectures("generale", ["LUN 16-18", "MER 17-18", "VEN 16-18"], ["Amaldi"])

schedule.add_lectures("bio", ["MAR 8-10"], ["Careri"])
schedule.add_lectures("bio", ["GIO 8-10"], ["Rasetti"])
schedule.add_lectures("bio", ["VEN 15-16"], ["Conversi"])

schedule.add_lectures("physlab.1", ["MAR 10-12"], ["Majorana"])
schedule.add_lectures("physlab.1", ["LUN 16-18"], ["Rasetti"])

schedule.add_lectures("physlab.2", ["LUN 14-16"], ["Aula6"])
schedule.add_lectures("physlab.2", ["VEN 10-12"], ["Conversi"])

schedule.add_lectures("physlab.3", ["MAR 10-12", "VEN 16-18"], ["Rasetti"])

schedule.add_lectures("compmet.1", ["LUN 9-12"], ["LabCalc"])
schedule.add_lectures("compmet.1", ["MAR 12-14", "VEN 14-16"], ["Aula6"])

schedule.add_lectures("compmet.2", ["LUN 12-16", "VEN 12-14"], ["LabCalc"])

schedule.add_lectures("compmet.3", ["LUN 8-10", "MAR 13-14", "MER 8-10"], ["Careri"])

schedule.add_lectures("compmet.4", ["LUN 15-16", "VEN 16-18"], ["Aula4"])
schedule.add_lectures("compmet.4", ["MER 14-16"], ["Rasetti"])
schedule.add_lectures("compmet.4", ["VEN 10-12"], ["LabCalc"])

schedule.add_lectures("softmatter", ["LUN 11-12", "MER 12-14", "GIO 12-14"], ["Rasetti"])

schedule.add_lectures("nlquantumoptics", ["LUN 18-19", "GIO 10-12"], ["Conversi"])
schedule.add_lectures("nlquantumoptics", ["MAR 12-14"], ["Rasetti"])

schedule.add_lectures("superiore", ["MER 10-12", "MAR 8-10", "VEN 14-15"], ["Rasetti"])

schedule.add_lectures("processi", ["LUN 12-14", "GIO 10-12"], ["Rasetti"])
schedule.add_lectures("processi", ["VEN 8-9"], ["Conversi"])

schedule.add_lectures("astrolab", ["MAR 16-18", "GIO 16-18", "VEN 12-13"], ["Rasetti"])
schedule.add_lectures("biochem", ["LUN 16-18", "MER 14-16"], ["Careri"])
schedule.add_lectures("biochem", ["VEN 14-15"], ["Conversi"])

# M2
schedule.add_lectures("complex", ["MER 12-13", "GIO 14-16", "VEN 12-14"], ["Careri"])
schedule.add_lectures("quantinfo", ["MAR 8-10"], ["Majorana"])
schedule.add_lectures("quantinfo", ["MER 13-14", "GIO 12-14"], ["Careri"])
schedule.add_lectures("stoca", ["LUN 14-16", "MER 16-18", "VEN 15-16"], ["Rasetti"])
schedule.add_lectures("spectra", ["LUN 14-16", "MER 16-18", "GIO 13-14"], ["Aula7"])
schedule.add_lectures("quantumfield", ["LUN 8-10", "VEN 8-10"], ["Rasetti"])
schedule.add_lectures("quantumfield", ["MER 12-13"], ["Conversi"])
# qui mancano queste ripetizioni, che però sono solo 2 ore
# schedule.add_lectures("quantumfield", ["MER 13-14 10-11"], ["Careri"])

schedule.add_lectures("disordered", ["LUN 10-12"], ["Conversi"])
schedule.add_lectures("disordered", ["VEN 10-12"], ["Rasetti"])
# qui mancano queste ripetizioni, che però sono solo 2 ore
schedule.add_lectures("disordered", ["MER 14-15"], ["Conversi"])

schedule.add_lectures("super", ["MER 10-12", "GIO 8-10"], ["Careri"])
schedule.add_lectures("super", ["LUN 15-16"], ["Careri"])

schedule.add_lectures("surface", ["LUN 14-15", "MAR 16-18", "MER 16-18"], ["Aula4"])

schedule.add_lectures("weak", ["LUN 14-16", "MER 16-18"], ["Aula3"])
schedule.add_lectures("weak", ["GIO 15-16"], ["Aula4"])

schedule.add_lectures("expgrav", ["LUN 14-16", "MAR 10-11", "VEN 8-10"], ["Aula8"])

schedule.add_lectures("intrograv", ["MAR 12-14", "MER 15-16"], ["Conversi"])
schedule.add_lectures("intrograv", ["GIO 12-14"], ["Majorana"])

schedule.add_lectures("labcalcadv", ["MER 10-12"], ["Aula8"])
schedule.add_lectures("labcalcadv", ["VEN 14-16"], ["Aula8"])
schedule.add_lectures("labcalcadv", ["VEN 16-18"], ["Lab1MAT"])

schedule.add_lectures("spaceastro", ["MAR 14-16", "GIO 14-15"], ["Rasetti"])
schedule.add_lectures("spaceastro", ["LUN 12-14"], ["Careri"])

schedule.add_lectures("astropart", ["LUN 8-10", "GIO 16-18"], ["Aula6"])

schedule.add_lectures("evoluzione", ["MER 16-18"], ["Careri"])
schedule.add_lectures("evoluzione", ["VEN 12-14"], ["Conversi"])

schedule.add_lectures("current", ["LUN 10-12", "MAR 11-12", "MER 16-18"], ["Aula8"])

schedule.add_lectures("medical", ["GIO 16-18"], ["Aula4"])
schedule.add_lectures("medical", ["MAR 16-17", "VEN 16-18"], ["Aula7"])

schedule.add_lectures("autograv", ["MAR 16-18", "GIO 16-18"], ["Aula8"])

schedule.add_lectures("cosmo", ["LUN 16-18", "VEN 14-16"], ["Aula4"])

schedule.add_lectures("collider", ["MAR 14-16", "MER 13-14", "GIO 8-10"], ["Aula8"])

schedule.add_lectures("intropart", ["MER 8-10", "GIO 10-12", "VEN 10-11"], ["Aula8"])

schedule.add_lectures("solidsensors", ["MER 12-14"], ["Aula3"])
schedule.add_lectures("solidsensors", ["GIO 13-14"], ["Aula6"])
schedule.add_lectures("solidsensors", ["MAR 14-16"], ["Aula7"])

# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])
schedule.add_lectures("studenti", ["LUN 14-19", "MAR 14-19", "MER 14-19", "GIO 14-19", "VEN 14-19"], ["Majorana"])
schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2", "Aula5"])

schedule.export_html("2022/v2_orario.html")
schedule.report("2022/v2_report.html")
